
-- 1. customerName of customers from PH
SELECT customerName FROM customers WHERE country = "Philippines";

-- 2. contactLastName and contactFirstName of "La Rochelle Gifts" customer
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

-- 3. product name and MSRP of "The Titanic" product
SELECT productName, MSRP FROM products WHERE productName LIKE "%The Titanic%";

-- 4. first and last name of employee with "jfirrelli@classicmodelcars.com"
SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

-- 5. names customers with no registered state
SELECT customerName FROM customers WHERE state IS NULL;

-- 6. firstName, lastName and email of Steve Patterson
SELECT firstName, lastName, email FROM employees WHERE firstName = "Steve" AND lastName = "Patterson";

-- 7. customerName, country and credit limit of customers not from USA and has credit limits greater than 3000
SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

-- 8. customer numbers of orders whose comment contains DHL
SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

-- 9. product lines with "state of the art" mentioned in their text description
SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

-- 10. all unique customer countries
SELECT DISTINCT country FROM customers;

-- 11. all unique statuses of orders
SELECT DISTINCT status FROM orders;

-- 12. customerName and country of customers from USA, France, Canada
SELECT customerName, country FROM customers WHERE country IN ("USA", "France", "Canada");

-- 13. first name, last name, and office's city of employees whose office are in Tokyo
SELECT employees.firstName, employees.lastName, offices.city FROM employees 
	JOIN offices ON employees.officeCode = offices.officeCode 
	WHERE city = "Tokyo";

-- 14. customerName of customers who were served by the employee "Leslie Thompson"
SELECT customers.customerName FROM customers 
	JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber 
	WHERE lastName = "Thompson" AND firstName = "Leslie";

-- 15. product names and customer name, of products ordered by "Baane Mini Imports"
SELECT products.productName, customers.customerName FROM customers
	JOIN orders ON customers.customerNumber = orders.customerNumber
	JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
	JOIN products ON orderdetails.productCode = products.productCode
	WHERE customerName = "Baane Mini Imports";

-- 16. employee's firstName, lastName, customer's names, and offices' countries, of transactions whose customers and offices are located in the same country
SELECT /*orders.orderNumber,*/ employees.firstname, employees.lastName, customers.customerName, offices.country FROM orders 
	JOIN customers ON orders.customerNumber = customers.customerNumber
	JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
	JOIN offices ON employees.officeCode = offices.officeCode 
	WHERE customers.country = offices.country;

-- 17. product name and quantity in stock, of products that belong to the product line "Planes" with stock quantities less than 1000
SELECT products.productName, products.quantityInStock FROM products
	JOIN productlines ON products.productLine = productlines.productLine
	WHERE productlines.productLine = "Planes" AND products.quantityInStock < 1000;

-- 18. customer's name with phone number containing "+81"
SELECT customerName FROM customers WHERE phone LIKE "%+81%";